package com.example.JpaExampleCar.Service;

import com.example.JpaExampleCar.Model.Car;
import com.example.JpaExampleCar.Repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CarService {
    @Autowired
    CarRepository carRepositoryObject;
    public String addCar(Car carObject) {
        carRepositoryObject.save(carObject);
        return "Car Details are added!!";
    }

    public Optional<Car> getCarById(int carID) {
        return carRepositoryObject.findById(carID);
    }

    public List<Car> getCarByNames(String carName) {
        return carRepositoryObject.findByCarName(carName);
    }

    public void deleteCar(int carID) {
        carRepositoryObject.deleteById(carID);
    }

    public List<Car> getCar() {
        return carRepositoryObject.findAll();
    }
}
