package com.example.JpaExampleCar.Service;

import com.example.JpaExampleCar.Model.Role;
import com.example.JpaExampleCar.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepositoryObject;
    public String addRole(Role roleObject) {
         roleRepositoryObject.save(roleObject);
         return "Role got added!!";
    }

    public List<Role> getRole() {
        return roleRepositoryObject.findAll();
    }
}
