package com.example.JpaExampleCar.Service;

import com.example.JpaExampleCar.Model.Role;
import com.example.JpaExampleCar.Model.User;
import com.example.JpaExampleCar.Repository.RoleRepository;
import com.example.JpaExampleCar.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepositoryObject;
    @Autowired
    RoleRepository roleRepositoryObject;

        public User addUsers(User userObject) {
           // userObject.setPassword(this.passwordEncoder.encode(userObject.getPassword()));


            //Here a list of users is created by fetching from the table
            List<User> userList=userRepositoryObject.findAll();
            if (!userList.isEmpty()){//if user available then make role as user
                Role roles=roleRepositoryObject.findById(3).orElseThrow();
                Set<Role> roleObj=new HashSet<>();
                roleObj.add(roles);
                //This below line adds the entry in join table
                userObject.setRoles(roleObj);
                return userRepositoryObject.save(userObject);
            }
            else {//if user not available then make role as admin
                //Line 36 to 46 is not needed if data is already present in role table
                Role roleobj1=new Role();
                roleobj1.setRoleName("admin");
                roleRepositoryObject.save(roleobj1);

                Role roleobj2=new Role();
                roleobj2.setRoleName("manager");
                roleRepositoryObject.save(roleobj2);

                Role roleobj3=new Role();
                roleobj3.setRoleName("user");
                roleRepositoryObject.save(roleobj3);

                Role roles=roleRepositoryObject.findById(1).orElseThrow();
                //by default first user role is admin
                Set<Role> roleObj=new HashSet<>();
                roleObj.add(roles);
                //Entry is getting added to join table which is user_roles
                userObject.setRoles(roleObj);
                return userRepositoryObject.save(userObject);
            }
        }

    public Optional<User> getUser(int userId) {
        return userRepositoryObject.findById(userId);
    }

    }





