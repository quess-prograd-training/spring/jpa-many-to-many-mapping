package com.example.JpaExampleCar.Controller;

import com.example.JpaExampleCar.Model.Car;
import com.example.JpaExampleCar.Service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    @Autowired
    CarService carServiceObject;

    @PostMapping("/inserting")
    public String addCar(@RequestBody Car carObject){
        String stringObj = carServiceObject.addCar(carObject);
        return stringObj;
    }

    @GetMapping("/fetchingAll")
    public List<Car> getCar(){
        return carServiceObject.getCar();
    }

    @GetMapping("/fetchingByName/{carName}")
    public List<Car> getCarByName(@PathVariable String carName){
        return carServiceObject.getCarByNames(carName);
    }

    @DeleteMapping("/deleting/{carID}")
    public void delete(@PathVariable int carID){
        carServiceObject.deleteCar(carID);
    }
}
