package com.example.JpaExampleCar.Controller;

import com.example.JpaExampleCar.Model.Role;
import com.example.JpaExampleCar.Service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleController {
    @Autowired
    RoleService roleServiceObject;

    @PostMapping("/insertingRoles")
    public String addRole(@RequestBody Role roleObject){
        String stringObj = roleServiceObject.addRole(roleObject);
        return stringObj;
    }

    @GetMapping("/fetching")
    public List<Role> getRoles(){
        return roleServiceObject.getRole();
    }
    }
