package com.example.JpaExampleCar.Controller;

import com.example.JpaExampleCar.Model.User;
import com.example.JpaExampleCar.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserController {
    @Autowired
    UserService userServiceObject;

    @PostMapping("/adding")
    public String addUser(@RequestBody User userObject ){
        String str = String.valueOf(userServiceObject.addUsers(userObject));
        return str;
    }

    @GetMapping("/getUserById/{userId}")
    public Optional<User> display(@PathVariable int userId){
        return userServiceObject.getUser(userId);
    }
}
