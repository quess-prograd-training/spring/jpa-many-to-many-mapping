package com.example.JpaExampleCar.Security;

import com.example.JpaExampleCar.Model.User;
import com.example.JpaExampleCar.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService  implements UserDetailsService {
    @Autowired
    private UserRepository userRepositoryObject;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userObject = userRepositoryObject.findByuserName(username);
        return new MyUserDetails(userObject);
    }
}
