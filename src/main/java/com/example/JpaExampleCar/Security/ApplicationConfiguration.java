package com.example.JpaExampleCar.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@EnableWebSecurity
@Configuration
public class ApplicationConfiguration  {
    //This method is used for creation of iobject for UserService class

    @Autowired
    MyUserDetailsService myUserDetailsObject;



    //This method is created for encrypting the passwords which are in database
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    public DaoAuthenticationProvider authenticationprovider(){
        DaoAuthenticationProvider authProviderObject = new DaoAuthenticationProvider();
        authProviderObject.setPasswordEncoder(passwordEncoder());
        authProviderObject.setUserDetailsService(myUserDetailsObject);
        return authProviderObject;
    }


   /* @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests((authz) -> authz
                        .anyRequest().authenticated()
                )
                .httpBasic(withDefaults());
        return http.build();
       /* http
                .sessionManagement()//for session management ie to delete cookies
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)//SessionCreationPolicy. STATELESS – Spring Security will never create a HttpSession and it will never use it to get the SecurityContext
                .and()//add() is acting like a bridge we have to apply here to connect

                .csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers("/fetchingAll").permitAll()//not authentication and not authorizaton neede antMatchers("/product/delete/**").hasAuthority("admin")
                .requestMatchers("/inserting").hasAuthority("admin")
                .anyRequest().authenticated()
                .and()
                .formLogin()

                .permitAll()
                .and().sessionManagement().maximumSessions(2).maxSessionsPreventsLogin(true);

        //.and().csrf().disable()
                //.logout().permitAll()
                //.and()
                //.exceptionHandling().accessDeniedPage("/product/error");
        http.authenticationProvider(authenticationprovider());*/

       // return http.build();*/
    }

