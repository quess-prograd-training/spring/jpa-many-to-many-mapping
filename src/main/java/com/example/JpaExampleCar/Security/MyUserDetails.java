package com.example.JpaExampleCar.Security;

import com.example.JpaExampleCar.Model.Role;
import com.example.JpaExampleCar.Model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class MyUserDetails implements UserDetails {

    private User userObject;

    public MyUserDetails(User userObject) {
        this.userObject = userObject;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<Role> roleSet = userObject.getRoles();
        //in line 24 we have fetched the roles from user class and line 25 to 29
        //we are fetching it from the set and storing back again in the SimpleGrantedAuthority object which is in built class in security dependency

        List<SimpleGrantedAuthority> authoritiesObject = new ArrayList<>();
        for(Role roleFor : roleSet){
            authoritiesObject.add(new SimpleGrantedAuthority(roleFor.getRoleName()));
        }
        return authoritiesObject;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
