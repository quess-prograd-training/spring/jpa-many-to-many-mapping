package com.example.JpaExampleCar.Repository;

import com.example.JpaExampleCar.Model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car,Integer> {
    List<Car> findByCarName(String carName);
}
