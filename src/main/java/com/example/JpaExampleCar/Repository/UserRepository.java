package com.example.JpaExampleCar.Repository;

import com.example.JpaExampleCar.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByuserName(String userName);
}
