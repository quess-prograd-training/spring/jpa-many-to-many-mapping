package com.example.JpaExampleCar.Repository;

import com.example.JpaExampleCar.Model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    Role findByroleName(Role roleName);
}
