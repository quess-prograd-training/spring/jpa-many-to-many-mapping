package com.example.JpaExampleCar.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Car {
    @Id
    private int carID;
    private String carName;
    private int carPrice;

    public Car() {
    }

    public Car(int carID, String carName, int carPrice) {
        this.carID = carID;
        this.carName = carName;
        this.carPrice = carPrice;
    }

    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public int getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(int carPrice) {
        this.carPrice = carPrice;
    }
}
